print(__doc__)

# Code source: Gael Varoquaux
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import os, csv
from sklearn import linear_model, datasets
from sklearn import model_selection

# import some data to play with
iris = datasets.load_iris()
X = iris.data
Y = iris.target
X_train = X[:40]
X_test = X[40:]
Y_train = Y[:40]
Y_test = Y[40:]
X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y,test_size=.2)

C_MIN = float(os.environ['C_MIN'])
C_MAX = float(os.environ['C_MAX'])

scores = []
for c in np.arange(C_MIN, C_MAX, .01):
        for p in ['l1', 'l2']:
            logreg = linear_model.LogisticRegression(C=c, penalty=p)
            # we create an instance of Neighbours Classifier and fit the data.
            logreg.fit(X_train, Y_train)
            score = logreg.score(X_test, Y_test, sample_weight=None)
            scores.append({'C': c, 'Penalty': p, 'Score': score})

csvpath = os.environ['CSV_PATH']
with open(csvpath, 'w') as csvfile:
        fieldnames = ['C', 'Penalty', 'Score']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in scores:
                writer.writerow(row)
