import os

os.system('docker build -t profiling:latest . ')

for i in range(10):
        C_MIN = i + .01
        C_MAX = (i+1)*1
        CSV_PATH = 'lr-{i}.csv'.format(i=i)
        os.system('docker run -e C_MIN={C_MIN} -e C_MAX={C_MAX} -e CSV_PATH={CSV_PATH} --name profiling-{i} profiling:latest python3 lr.py'.format(C_MIN=C_MIN, C_MAX=C_MAX, CSV_PATH=CSV_PATH, i=i))
	os.system('docker cp profiling-{i}:app/{CSV_PATH} ./results/'.format(CSV_PATH=CSV_PATH, i=i))
        os.system('docker rm profiling-{i}'.format(i=i))
