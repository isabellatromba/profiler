FROM ubuntu:16.04

RUN apt-get -y --fix-missing update
RUN apt-get -y install python python3-pip
RUN apt-get -y install git

RUN pip3 install --upgrade pip

RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app
