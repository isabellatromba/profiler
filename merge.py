import glob
import csv
import os

os.system('head -1 results/lr-0.csv > merged.csv')
for file in glob.glob('./results/*.csv'):
    os.system('awk "NR>1" {file} >> merged.csv'.format(file=file))
